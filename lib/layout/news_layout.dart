import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../modules/search_news/search_screen.dart';
import '../shared/app_cubit/cubit.dart';
import '../shared/components/components.dart';
import '../shared/cubit/cubit.dart';
import '../shared/cubit/states.dart';

class NewsLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NewsAppCubit, NewsAppCubitStates>(
        listener: (context, state) {},
        builder: (context, state) {
          var Cubit = NewsAppCubit.get(context);
          return Scaffold(
              appBar: AppBar(
                title: Text(
                  "News App",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                actions: [
                  IconButton(
                    icon: Icon(Icons.search),
                    onPressed: () {
                      navigateTo(context, SearchScreen());
                    },
                  ),
                  IconButton(
                      onPressed: () {
                        AppCubit.get(context).ChangeAppMode();
                      },
                      icon: Icon(Icons.brightness_4_outlined))
                ],
              ),
              body: Cubit.Screens[Cubit.CurrentIndex],
              bottomNavigationBar: BottomNavigationBar(
                currentIndex: Cubit.CurrentIndex,
                onTap: (index) {
                  Cubit.ChangeBottomNavigationBarIndex(index);
                },
                items: Cubit.Items,
              ));
        });
  }
}
