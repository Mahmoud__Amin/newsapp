
import 'package:NewsApp/shared/app_cubit/cubit.dart';
import 'package:NewsApp/shared/app_cubit/states.dart';
import 'package:NewsApp/shared/blocobserver.dart';
import 'package:NewsApp/shared/cubit/cubit.dart';
import 'package:NewsApp/shared/network/local/cache_helper.dart';
import 'package:NewsApp/shared/network/remote/dio_helper.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hexcolor/hexcolor.dart';
import 'layout/news_layout.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  /*// set minimum width and height on the app
  if (Platform.isMacOS) {
    await DesktopWindow.setMinWindowSize(Size(
        650.0,
        650.0
    ));
  }*/

  await CacheHelper.init();
  bool? isDark = CacheHelper.getBoolean(key: "isDark");
  DioHelper.init();
  BlocOverrides.runZoned(
        () =>
    {

      runApp(MyApp(isDark)),
    },
    blocObserver: MyBlocObserver(),
  );
}

class MyApp extends StatelessWidget {
  final bool? isDark;

  MyApp(this.isDark);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
            create: (context) =>
            NewsAppCubit()
              ..getBusiness()
              ..getSports()
              ..getScience()),
        BlocProvider(
          create: (BuildContext context) =>
          AppCubit()
            ..ChangeAppMode(
                fromShared: isDark
            ),
        )
      ],
      child: BlocConsumer<AppCubit, AppStates>(
        builder: (BuildContext context, state) {
          return MaterialApp(
            theme: ThemeData(
                appBarTheme: AppBarTheme(
                    color: Colors.white,
                    titleSpacing: 20,
                    elevation: 0,
                    systemOverlayStyle: SystemUiOverlayStyle(
                        statusBarColor: Colors.white,
                        statusBarIconBrightness: Brightness.dark),
                    titleTextStyle: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                    iconTheme: IconThemeData(color: Colors.black)),
                progressIndicatorTheme:
                ProgressIndicatorThemeData(color: Colors.deepOrange),
                primaryColor: Colors.deepOrange,
                inputDecorationTheme: InputDecorationTheme(
                  labelStyle: TextStyle(color: Colors.deepOrange),
                  prefixIconColor: Colors.deepOrange,
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.deepOrange),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.deepOrange),
                  ),
                ),
                floatingActionButtonTheme: FloatingActionButtonThemeData(
                    backgroundColor: Colors.deepOrangeAccent),
                bottomNavigationBarTheme: BottomNavigationBarThemeData(
                  type: BottomNavigationBarType.fixed,
                  selectedItemColor: Colors.deepOrangeAccent,
                  unselectedItemColor: Colors.grey,
                ),
                scaffoldBackgroundColor: Colors.white,
                textTheme: TextTheme(
                    bodyText1: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w600)),
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(color: Colors.deepOrange)),
            darkTheme: ThemeData(
                appBarTheme: AppBarTheme(
                    titleSpacing: 20,
                    color: HexColor('333739'),
                    elevation: 0,
                    systemOverlayStyle: SystemUiOverlayStyle(
                        statusBarColor: HexColor('333739'),
                        statusBarIconBrightness: Brightness.light),
                    titleTextStyle: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                    iconTheme: IconThemeData(color: Colors.white)),
                progressIndicatorTheme:
                ProgressIndicatorThemeData(color: Colors.deepOrange),
                inputDecorationTheme: InputDecorationTheme(
                  labelStyle: TextStyle(color: Colors.deepOrange),
                  prefixIconColor: Colors.deepOrange,
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.deepOrange),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.deepOrange),
                  ),
                ),
                floatingActionButtonTheme: FloatingActionButtonThemeData(
                    backgroundColor: Colors.deepOrangeAccent),
                bottomNavigationBarTheme: BottomNavigationBarThemeData(
                  type: BottomNavigationBarType.fixed,
                  backgroundColor: HexColor('333739'),
                  selectedItemColor: Colors.deepOrangeAccent,
                  unselectedItemColor: Colors.grey,
                ),
                textTheme: TextTheme(
                    bodyText1: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600)),
                scaffoldBackgroundColor: HexColor('333739'),
                backgroundColor: Colors.white),
            themeMode: AppCubit
                .get(context)
                .isDark ? ThemeMode.dark : ThemeMode.light,
            debugShowCheckedModeBanner: false,
            home: NewsLayout(),



          );
        },
        listener: (BuildContext context, Object? state) {},
      ),
    );
  }
}
