import 'package:NewsApp/modules/web_view_screen/web_view_screen.dart';
import 'package:NewsApp/shared/cubit/cubit.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';

import '../cubit/cubit.dart';
Widget BuildArticleItem(item, context,index) {

  return Container(
    color: (index==NewsAppCubit.get(context).selectedIndex && NewsAppCubit.get(context).isDesktop)?Colors.grey[200]:null,
    child: InkWell(
      onTap:(){
           NewsAppCubit.get(context).selectIndex(index);
           /*navigateTo(context, WebViewScreen(item['url']));*/
      },
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Row(
          children: [
            Container(
              width: 120,
              height: 120,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  image: DecorationImage(
                      image: NetworkImage(item['urlToImage'] != null
                          ? '${item['urlToImage']}'
                          : 'https://www.google.com/imgres?imgurl=https%3A%2F%2Fmedia.mixbook.com%2Fimages%2Ftemplates%2F97_1_0_m.jpg&imgrefurl=https%3A%2F%2Fwww.mixbook.com%2Fall-photo-books%2Fblank-canvas-10&tbnid=u6NPMJiu26m2lM&vet=12ahUKEwjvp8OG8fb2AhWXaPEDHWvcBVgQMygAegUIARDfAQ..i&docid=1CDFhU69Q04dVM&w=300&h=301&q=blank%20photo&client=firefox-b-d&ved=2ahUKEwjvp8OG8fb2AhWXaPEDHWvcBVgQMygAegUIARDfAQ'),
                      fit: BoxFit.cover)),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              child: Container(
                height: 120,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Text(
                        '${item['title']}',
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: Theme
                            .of(context)
                            .textTheme
                            .bodyText1,
                      ),
                    ),
                    Text('${item['publishedAt']}',
                        style: TextStyle(color: Colors.grey)),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    ),
  );
}

Widget BuildNewsPage(items,bool isSearchScreen) {
  return ConditionalBuilder(
    builder: (BuildContext context) =>
        ListView.separated(
            itemBuilder: (BuildContext context, int index) {
              return BuildArticleItem(items[index], context,index);
            },
            separatorBuilder: (BuildContext context, int index) => Divider(),
            itemCount: items.length),
    condition: items.length > 0,
    fallback: (BuildContext context) => (!isSearchScreen)?Center(child: CircularProgressIndicator()):Container(),
  );
}

Widget Divider() {
  return Padding(
    padding: const EdgeInsetsDirectional.only(
      start: 20.0,
    ),
    child: Container(
      width: double.infinity,
      height: 1.0,
      color: Colors.grey[300],
    ),
  );
}

void navigateTo(BuildContext context, Widget widget) {
  Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => widget));
}

  Widget BuildTextFromField({
     TextEditingController? controller,
     TextInputType? textInputType,
     String? validatorString,
     String? labelText,
     IconData? prefixIcon

  }) {
    return TextFormField(
      controller: controller,
      keyboardType: textInputType,
      validator: (String? value) {
        if (value!.isEmpty) {
          return '${validatorString}}';
        }
        return null;
      },
      decoration: InputDecoration(
        label: Text("${labelText}"),
        prefix: Icon(prefixIcon),
      ));
  }

