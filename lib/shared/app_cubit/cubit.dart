
import 'package:NewsApp/shared/app_cubit/states.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../network/local/cache_helper.dart';


class AppCubit extends Cubit<AppStates> {
  AppCubit() : super(AppInitialState());
  static AppCubit get(context) {
    return BlocProvider.of(context);
  }
  bool isDark=true ;
   void ChangeAppMode({bool? fromShared}) async {
    bool? temp=  CacheHelper.getBoolean(key: 'isDark');
    if(fromShared != null){
      isDark=fromShared;
      emit(AppChangeModeState());
    }
    else{
      isDark=!isDark;
       CacheHelper.setBoolean(key:"isDark",value:isDark).then((value) =>  emit(AppChangeModeState()));
    }

  }}