
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:NewsApp/shared/cubit/states.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../modules/business_news/business_news_screen.dart';
import '../../modules/science_news/science_news_screen.dart';
import '../../modules/settings_news/settings_news_screen.dart';
import '../../modules/sports_news/sports_news_screen.dart';
import '../network/remote/dio_helper.dart';

class NewsAppCubit extends Cubit<NewsAppCubitStates> {
  NewsAppCubit() : super(NewsAppInitialState());

  static NewsAppCubit get(context) => BlocProvider.of(context);
  List<BottomNavigationBarItem> Items = [
    BottomNavigationBarItem(icon: Icon(Icons.business), label: "Business"),
    BottomNavigationBarItem(icon: Icon(Icons.sports), label: "Sports"),
    BottomNavigationBarItem(icon: Icon(Icons.science), label: "Science"),
  ];
  int CurrentIndex = 0;
  List Screens = [
    BusinessNewsScreen(),
    SportsNewsScreen(),
    ScienceNewsScreen(),
  ];
  int selectedIndex=0;
  void ChangeBottomNavigationBarIndex(index) {
    CurrentIndex = index;
    emit(NewsAppBottomNavigationBarState());
  }
  void selectIndex(int index){
    selectedIndex=index;
    emit(NewsAppSelectItemState());
  }
  bool isDesktop=false;
  void changeDesktopState(bool value){
    isDesktop=value;
    emit(NewsAppChangeDesktopState());
  }
  List<dynamic> Business=[];

   void  getBusiness() {
     emit(NewsAppProgressIndicatorBusinessState());
     DioHelper.getData(url: "v2/top-headlines", query: {
      'country': 'eg',
      'category': 'business',
      'apiKey': '331a163f86694d08ace9baabe9dfc787'
    })?.then((value){
      Business=value?.data['articles'];
      emit(NewsAppGetBusinessState());
      //print(Business.length);
    }).catchError((error){
       print(error.toString());
       emit(NewsAppOnErrorBusinessState(error.toString()));
    });
  }
  List<dynamic> Sports=[];
  void  getSports() {
     emit(NewsAppProgressIndicatorSportsState());
      DioHelper.getData(url: "v2/top-headlines", query: {
      'country': 'eg',
      'category': 'sports',
      'apiKey': '331a163f86694d08ace9baabe9dfc787'
    })?.then((value){
      Sports=value?.data['articles'];
      emit(NewsAppGetSportsState());

    }).catchError((error){
        print(error.toString());
        emit(NewsAppOnErrorSportsState(error.toString()));
    });
  }
  List<dynamic> Science=[];
   void  getScience() {
    emit(NewsAppProgressIndicatorScienceState());
    DioHelper.getData(url: "v2/top-headlines", query: {
      'country': 'eg',
      'category': 'science',
      'apiKey': '331a163f86694d08ace9baabe9dfc787'
    })?.then((value){
      Science=value?.data['articles'];
      //print(Science.length);
      emit(NewsAppGetScienceState());
    }).catchError((error){
      print(error.toString());
      emit(NewsAppOnErrorScienceState(error.toString()));

    });
  }


  List<dynamic> SearchData=[];
  void  getSearchData(String search) {
    emit(NewsAppProgressIndicatorSearchState());
    DioHelper.getData(url: "v2/everything", query: {
      'q':'${search}',
      'apiKey': '331a163f86694d08ace9baabe9dfc787'
    })?.then((value){
      SearchData=value?.data['articles'];
      //print(Science.length);
      emit(NewsAppGetSearchState());

    }).catchError((error){
      print(error.toString());
      emit(NewsAppOnErrorSearchState(error.toString()));

    });
  }
  // print(isDark.toString());
   }



