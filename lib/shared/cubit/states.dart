abstract class NewsAppCubitStates {}

class NewsAppInitialState extends NewsAppCubitStates{}

class NewsAppBottomNavigationBarState extends NewsAppCubitStates{}

class NewsAppGetBusinessState extends NewsAppCubitStates{}

class NewsAppGetSportsState extends NewsAppCubitStates{}

class NewsAppGetScienceState extends NewsAppCubitStates{}

class NewsAppGetSearchState extends NewsAppCubitStates{}

class NewsAppProgressIndicatorBusinessState extends NewsAppCubitStates{}

class NewsAppProgressIndicatorSportsState extends NewsAppCubitStates{}

class NewsAppProgressIndicatorScienceState extends NewsAppCubitStates{}

class NewsAppProgressIndicatorSearchState extends NewsAppCubitStates{}

class NewsAppWebViewState extends NewsAppCubitStates{}

class NewsAppOnErrorBusinessState extends NewsAppCubitStates{
  final String error;

  NewsAppOnErrorBusinessState(this.error);

}

class NewsAppOnErrorSportsState extends NewsAppCubitStates{
  final String error;

  NewsAppOnErrorSportsState(this.error);
}

class NewsAppOnErrorScienceState extends NewsAppCubitStates{
  final String error;

  NewsAppOnErrorScienceState(this.error);
}

class NewsAppOnErrorSearchState extends NewsAppCubitStates {
  final String error;

  NewsAppOnErrorSearchState(this.error);

}
class NewsAppSelectItemState extends NewsAppCubitStates{}

class NewsAppChangeDesktopState extends NewsAppCubitStates{}


