import 'package:NewsApp/shared/components/components.dart';
import 'package:NewsApp/shared/cubit/cubit.dart';
import 'package:NewsApp/shared/cubit/states.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ScienceNewsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NewsAppCubit, NewsAppCubitStates>(
        listener: (BuildContext context, Object? state) {},
        builder: (BuildContext context, state) {
          var item = NewsAppCubit.get(context).Science;
          return BuildNewsPage(item,false);
        });
  }
}
