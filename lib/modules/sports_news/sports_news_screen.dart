import 'package:NewsApp/shared/components/components.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../shared/cubit/cubit.dart';
import '../../shared/cubit/states.dart';

class SportsNewsScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NewsAppCubit, NewsAppCubitStates>(
        listener: (BuildContext context, Object? state) {},
        builder: (BuildContext context, state) {
          var item = NewsAppCubit.get(context).Sports;
          return BuildNewsPage(item,false);
        });
  }
}
