import 'package:NewsApp/shared/components/components.dart';
import 'package:NewsApp/shared/cubit/cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../shared/cubit/states.dart';

class SearchScreen extends StatelessWidget {
  TextEditingController SearchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NewsAppCubit, NewsAppCubitStates>(
        listener: (context, state) {},
        builder: (context, state) {
          var items = NewsAppCubit.get(context).SearchData;
          return Scaffold(
            appBar: AppBar(),
            body: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: TextFormField(
                    controller: SearchController,
                    style:Theme.of(context).textTheme.bodyText1,
                    keyboardType: TextInputType.text,
                    onChanged: (String value) {
                       NewsAppCubit.get(context).getSearchData(value);
                    },
                    cursorColor: Colors.deepOrange,
                    validator: (String? value) {
                      if (value!.isEmpty) {
                        return 'search must not be empty';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      label: Text("Search"),
                      prefixIcon: Icon(Icons.search),

                    ),
                  ),
                ),
                Expanded(child: BuildNewsPage(items,true)),
              ],
            ),
          );
        });
  }
}
