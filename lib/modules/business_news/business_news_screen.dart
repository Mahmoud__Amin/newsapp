import 'package:NewsApp/shared/components/components.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '../../shared/cubit/cubit.dart';
import '../../shared/cubit/states.dart';

class BusinessNewsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NewsAppCubit, NewsAppCubitStates>(
        listener: (BuildContext context, Object? state) {},
        builder: (BuildContext context, state) {
          var item = NewsAppCubit.get(context).Business;
          return ScreenTypeLayout(
            mobile: Builder(
                builder: (BuildContext context) {
                  NewsAppCubit.get(context).changeDesktopState(false);
                  return BuildNewsPage(item, false);
                }),
            desktop: Builder(
              builder: (BuildContext context) {
                NewsAppCubit.get(context).changeDesktopState(true);
               return  Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(child: BuildNewsPage(item, false)),
                    if(item.length>0)Expanded(child: Container(
                      height: double.infinity,
                      color: Colors.grey[200],
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Text(
                          "${item[NewsAppCubit.get(context).selectedIndex]['description']}",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                    ))
                  ],
                );
              },

            ),
            breakpoints: ScreenBreakpoints(
              desktop: 850,
              tablet: 600,
              watch: 100,
            ),
          );
        });
  }
}
